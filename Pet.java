
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Brandon Quach
 */
public class Pet {
    private String name;
    private String species;
  

    public Pet(String aName, String aSpecies){
        name = aName;
        species = aSpecies;
    }
    
    public void setName(String aName){
        name = aName;
    }
    public void setSpecies(String aSpecies){
        species = aSpecies;
    }
    public String getName(){
        return name;
    }
    public String getSpecies(){
        return species;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pet other = (Pet) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false; 
        }
        if (!Objects.equals(this.species, other.species)) {
            return false;
        }
        return true;
    }
    
    public String toString(){
        return name + " " + species;
    }
    
    

}
