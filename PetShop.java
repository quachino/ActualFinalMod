/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Queue;
import java.util.LinkedList;
import java.util.Scanner;
/**
 *
 * @author Brandon Quach
 */
public class PetShop {
    LinkedListQueue dogs = new LinkedListQueue();
    LinkedListQueue cats = new LinkedListQueue();
    LinkedListQueue animalShelter = new LinkedListQueue();
    
    public static void main(String[] args) {
    
    PetShop petShop = new PetShop();
    Scanner key = new Scanner(System.in);
    boolean exit = false;
        System.out.println("~~~Hello! Welcome to the adoption center!~~~ \n"
                + "Please choose an option");
        while(!exit){
            System.out.println("1.  Donate a Cat\n" +
                        "2.  Donate a Dog\n" +
                        "3.  Adopt a Cat\n" +
                        "4.  Adopt a Dog\n" +
                        "5.  Adopt Oldest Pet\n" +
                        "6. Exit");
            int userInput = key.nextInt();
            key.nextLine();
        
            switch(userInput){
            
            case 1: 
                petShop.donateCat();
                break;
            case 2:
                petShop.donateDog();
                break;
            case 3:
                petShop.adoptCat();
                break;
            case 4:
                petShop.adoptDog();
                break;
            case 5:
                petShop.adoptOldest();
                break;
            default:
                System.out.println("Later!");
                exit = true;
                break;
            
            }
        }
    
    }
 
    public void donateCat(){
       Scanner key = new Scanner(System.in);
       System.out.println("\nWhat is the name of the pet cat?");
       String petName = key.next();
       Pet petCat = new Pet(petName, "cat");
       cats.enqueue(petCat);
       animalShelter.enqueue(petCat);
       System.out.println("You donated a kitty! "+ petCat.getName());
   }
    public void donateDog(){
        Scanner key = new Scanner(System.in);
        System.out.println("\nWhat is the name of the pet dog?");
       String petName = key.next();
       Pet petDog = new Pet(petName, "dog");
       dogs.enqueue(petDog);
       animalShelter.enqueue(petDog);
       System.out.println("You donated a pupper! "+ petDog.getName());
      
    }
    public void adoptCat(){
         System.out.println("\nAdopting a cat...");
         if (cats.empty()){
             System.out.println("Sorry no cats left!");
             return;
         }
         Pet adoptedCat = (Pet) cats.dequeue();

         LinkedListQueue temp = new LinkedListQueue();

         Pet oldestPet = (Pet) animalShelter.dequeue();

         while(!adoptedCat.equals(oldestPet)){
             temp.enqueue(oldestPet);
             oldestPet = (Pet) animalShelter.dequeue();
         }
         System.out.println("You adopted "+ adoptedCat.getName() +"!");
   }

    public void adoptDog()
    {
        System.out.println("Adopting a dog...");
         if (dogs.empty()){
             System.out.println("Sorry no dogs left!");
             return;
         }
         Pet adoptedDog = (Pet) dogs.dequeue();

         LinkedListQueue temp = new LinkedListQueue();

         Pet oldestPet = (Pet) animalShelter.dequeue();

         while(!adoptedDog.equals(oldestPet)){
             temp.enqueue(oldestPet);
             oldestPet = (Pet) animalShelter.dequeue();
         }
         System.out.println("You adopted "+ adoptedDog.getName() +"!");
        
    }
    public void adoptOldest(){
        
        Pet adoptedPet = (Pet) animalShelter.dequeue();
        String speciesOfAdoptedPet = adoptedPet.getSpecies();
        if(speciesOfAdoptedPet.equals("cat")){
            cats.dequeue();
        } 
        else if(speciesOfAdoptedPet.equals("dog")){
            dogs.dequeue();
        }
        System.out.println("You adopted the " + speciesOfAdoptedPet + ", " + adoptedPet.getName() + "!");
    }
}
