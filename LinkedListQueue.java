/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.NoSuchElementException;

/**
 *
 * @author Brandon Quach
 */

public class LinkedListQueue
{
   private Node first;
   private Node last;
   
   /** 
      Constructs an empty queue.
   */
   public LinkedListQueue()
   {  
      first = null;
      last = null;
   }

   /**
      Adds an element to the top of the stack.
      @param element the element to add
   */
   public void enqueue(Object element)
   {  
      Node newNode = new Node();
      newNode.data = element;
      newNode.next = null;
      if (last != null)
      {
          last.next = newNode;
      }
      
      if (first == null)
      {
          first = newNode;
      }
      
      last = newNode;
   }

   /**
      Removes the element from the top of the stack.
      @return the removed element
   */
   public Object dequeue()
   {  
      if (first == null) { throw new NoSuchElementException(); }
      Object element = first.data;
      if (first == last)
      {
         last = null;
      }

      first = first.next;
      return element;
   }

   /**
      Checks whether this stack is empty.
      @return true if the stack is empty
   */
   public boolean empty()
   {
      return first == null;
   }
   
   class Node
   {  
      public Object data;
      public Node next;
   }
   public String toString(){
       Node current = first;
       String output = "";
       while(current != null)
       {
           output += current.data + ", ";
           current = current.next;
       }
       return output;
   }
}
